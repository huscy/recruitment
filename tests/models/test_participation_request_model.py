import pytest

from model_bakery import baker


@pytest.mark.django_db
def test_default_status():
    participation_request = baker.make('recruitment.ParticipationRequest')

    assert participation_request.status == 0
