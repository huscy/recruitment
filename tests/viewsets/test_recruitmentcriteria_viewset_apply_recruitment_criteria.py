from datetime import date
from itertools import cycle
from operator import itemgetter

import pytest
from freezegun import freeze_time
from model_bakery import baker

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN

from huscy.attributes.services import get_attribute_set

pytestmark = pytest.mark.django_db


@pytest.fixture
def subjects():
    ages = [17, 18, 22, 29, 31, 45, 55, 74, 81, 86]
    return baker.make(
        'subjects.Subject',
        contact__date_of_birth=cycle([date(2025 - age, 1, 1) for age in ages]),
        _quantity=len(ages)
    )


def test_admin_user_can_apply_recruitment_criteria(admin_client, recruitment_criteria):
    response = apply_recruitment_criteria(admin_client, recruitment_criteria)

    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_apply_recruitment_criteria(client, recruitment_criteria):
    response = apply_recruitment_criteria(client, recruitment_criteria)

    assert response.status_code == HTTP_200_OK


def test_anonymous_user_cannot_apply_recruitment_criteria(anonymous_client, recruitment_criteria):
    response = apply_recruitment_criteria(anonymous_client, recruitment_criteria)

    assert response.status_code == HTTP_403_FORBIDDEN


@freeze_time("2025-01-02")
def test_filter_subjects_with_initial_attribute_sets(client, subjects, recruitment_criteria):
    list(map(get_attribute_set, subjects))  # create initial attribute sets

    response = apply_recruitment_criteria(client, recruitment_criteria)

    # Return subjects with ages 18, 22, 29 and 31. Subject with age 45 is one day too young
    assert len(response.json()) == 4


@freeze_time("2025-01-02")
def test_filter_subjects(client, subjects, subject_group, attribute_schema):
    for index, subject in enumerate(subjects):
        attribute_set = get_attribute_set(subject)
        attribute_set.attributes['attribute'] = index
        attribute_set.save()

    recruitment_criteria = baker.make('recruitment.RecruitmentCriteria',
                                      subject_group=subject_group,
                                      minimum_age_in_months=18*12,
                                      maximum_age_in_months=80*12,
                                      attribute_filterset={
                                          'attribute': {
                                              'operator': 'range',
                                              'values': [[4, 9]],
                                          }
                                      })

    response = apply_recruitment_criteria(client, recruitment_criteria)

    results = response.json()
    assert len(results) == 4

    subject_ids = list(map(itemgetter('id'), results))
    assert str(subjects[4].id) in subject_ids
    assert str(subjects[5].id) in subject_ids
    assert str(subjects[6].id) in subject_ids
    assert str(subjects[7].id) in subject_ids


def apply_recruitment_criteria(client, recruitment_criteria):
    subject_group = recruitment_criteria.subject_group
    experiment = subject_group.experiment
    return client.get(
        reverse(
            'recruitmentcriteria-apply',
            kwargs=dict(
                project_pk=experiment.project.pk,
                experiment_pk=experiment.pk,
                subjectgroup_pk=subject_group.pk,
                pk=recruitment_criteria.pk,
            )
        )
    )
