import pytest

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN

pytestmark = pytest.mark.django_db


def test_admin_user_can_update_recruitment_criteria(admin_client, recruitment_criteria):
    response = update_recruitment_criteria(admin_client, recruitment_criteria)

    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_update_recruitment_criteria(client, recruitment_criteria):
    response = update_recruitment_criteria(client, recruitment_criteria)

    assert response.status_code == HTTP_200_OK, response.json()


def test_anonymous_user_cannot_update_recruitment_criteria(anonymous_client, recruitment_criteria):
    response = update_recruitment_criteria(anonymous_client, recruitment_criteria)

    assert response.status_code == HTTP_403_FORBIDDEN


def update_recruitment_criteria(client, recruitment_criteria):
    subject_group = recruitment_criteria.subject_group
    experiment = subject_group.experiment

    return client.put(
        reverse(
            'recruitmentcriteria-detail',
            kwargs=dict(
                project_pk=experiment.project.pk,
                experiment_pk=experiment.pk,
                subjectgroup_pk=subject_group.pk,
                pk=recruitment_criteria.pk,
            )
        ),
        data=dict(
            minimum_age_in_months=20,
            maximum_age_in_months=50,
            attribute_filterset={
                'attribute': [[0, 10]],
            },
        ),
        format='json'
    )
