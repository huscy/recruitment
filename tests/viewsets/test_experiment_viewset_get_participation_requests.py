import pytest

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN

pytestmark = [pytest.mark.django_db, pytest.mark.skip]


def test_admin_user_can_retrieve_participation_requests(admin_client, experiment):
    response = retrieve_participation_requests(admin_client, experiment)
    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_retrieve_participation_requests(client, experiment):
    response = retrieve_participation_requests(client, experiment)
    assert response.status_code == HTTP_200_OK


def test_anonymous_user_cannot_retrieve_participation_requests(anonymous_client, experiment):
    response = retrieve_participation_requests(anonymous_client, experiment)
    assert response.status_code == HTTP_403_FORBIDDEN


def retrieve_participation_requests(client, experiment):
    return client.get(reverse('experiment-participation-requests', kwargs=dict(pk=experiment.pk)))
