import pytest

from huscy.recruitment.services import update_recruitment_criteria

pytestmark = pytest.mark.django_db


def test_update_attribute_filterset(subject_group, recruitment_criteria):
    result = update_recruitment_criteria(subject_group, attribute_filterset={'attribute1': 5})

    subject_group.refresh_from_db()
    recruitment_criteria = subject_group.recruitment_criteria.get()
    assert recruitment_criteria == result
    assert recruitment_criteria.attribute_filterset == {'attribute1': 5}


def test_with_existing_participation_request(subject_group, recruitment_criteria,
                                             participation_request):
    result = update_recruitment_criteria(subject_group, attribute_filterset={'attribute1': 5})

    recruitment_criteria.refresh_from_db()
    assert recruitment_criteria.attribute_filterset == {}  # keep old recruitment criteria

    subject_group.refresh_from_db()
    recruitment_criteria = subject_group.recruitment_criteria.latest('id')
    assert recruitment_criteria == result
    assert recruitment_criteria.attribute_filterset == {'attribute1': 5}
