"""
import pytest

from model_bakery import baker

from huscy.recruitment.services import get_participation_requests

pytestmark = pytest.mark.django_db


def test_get_participation_requests_without_arguments():
    with pytest.raises(ValueError):
        get_participation_requests()


def test_get_participation_requests_by_project(attribute_filterset, participation_request):

    # check one exists by default
    assert 1 == len(get_participation_requests(attribute_filterset=attribute_filterset))

    # make 3 more for the same project and check there are 4
    baker.make('recruitment.ParticipationRequest', attribute_filterset=attribute_filterset,
               _quantity=3)
    assert 4 == len(get_participation_requests(attribute_filterset=attribute_filterset))

    # make 3 for different project and check there are still 4
    second_attribute_filterset = baker.make('recruitment.AttributeFilterSet')
    baker.make('recruitment.ParticipationRequest', attribute_filterset=second_attribute_filterset,
               _quantity=3)
    assert 4 == len(get_participation_requests(attribute_filterset=attribute_filterset))


def test_get_participation_requests_by_subject(subject, attribute_filterset,
                                               participation_request_content_type,
                                               participation_request):

    # check one exist by default
    assert 1 == len(get_participation_requests(subject=subject))

    # make one for different attribute filterset and check there is still 1
    baker.make('recruitment.ParticipationRequest', attribute_filterset=attribute_filterset,
               _quantity=3)
    assert 1 == len(get_participation_requests(subject=subject))

    # make one more participation request for the different attribute_filterset and check
    # there are 2
    second_attribute_filterset = baker.make('recruitment.AttributeFilterset')
    pseudonym = baker.make('pseudonyms.Pseudonym', subject=subject,
                           content_type=participation_request_content_type)
    baker.make('recruitment.ParticipationRequest', attribute_filterset=second_attribute_filterset,
               pseudonym=pseudonym.code)

    assert 2 == len(get_participation_requests(subject=subject))


def test_get_participation_requests_by_subject_and_attribute_filterset(
        subject, participation_request_content_type, participation_request):
    # check one exists by default
    assert 1 == len(get_participation_requests(
        subject=subject, attribute_filterset=participation_request.attribute_filterset))

    # make one for different attribute filterset and check there is 1
    second_attribute_filterset = baker.make('recruitment.AttributeFilterSet')
    baker.make('recruitment.ParticipationRequest', attribute_filterset=second_attribute_filterset)

    assert 1 == len(get_participation_requests(
        subject=subject, attribute_filterset=participation_request.attribute_filterset))

    # make one participation request for subject within diffrent attribute_filterset and check
    # there is 1
    pseudonym = baker.make('pseudonyms.Pseudonym', subject=subject,
                           content_type=participation_request_content_type)
    baker.make('recruitment.ParticipationRequest', attribute_filterset=second_attribute_filterset,
               pseudonym=pseudonym.code)

    assert 1 == len(get_participation_requests(
        subject=subject, attribute_filterset=participation_request.attribute_filterset))

    # make one participation request for subject for the same attribute_filterset and check
    # there are 2
    ps2 = baker.make('pseudonyms.Pseudonym', subject=subject,
                     content_type=participation_request_content_type)
    baker.make('recruitment.ParticipationRequest',
               attribute_filterset=participation_request.attribute_filterset,
               pseudonym=ps2.code)
    assert 2 == len(get_participation_requests(
        subject=subject, attribute_filterset=participation_request.attribute_filterset))
"""
