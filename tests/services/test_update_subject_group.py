from itertools import cycle

import pytest
from model_bakery import baker

from huscy.recruitment.services import update_subject_group
from huscy.recruitment.models import SubjectGroup

pytestmark = pytest.mark.django_db


def test_update_name(subject_group):
    update_subject_group(subject_group, name='new name')

    subject_group.refresh_from_db()
    assert subject_group.name == 'new name'
    assert subject_group.description == 'description'


def test_update_description(subject_group):
    update_subject_group(subject_group, description='new description')

    subject_group.refresh_from_db()
    assert subject_group.name == 'name'
    assert subject_group.description == 'new description'


def test_update_both_name_and_description(subject_group):
    update_subject_group(subject_group, name='new name', description='new description')

    subject_group.refresh_from_db()
    assert subject_group.name == 'new name'
    assert subject_group.description == 'new description'


@pytest.mark.parametrize("actual_order, new_order, expected_query", [
    (2, 5, [0, 1, 5, 2, 3, 4, 6]),
    (5, 2, [0, 1, 3, 4, 5, 2, 6]),
    (0, 0, [0, 1, 2, 3, 4, 5, 6]),
])
def test_update_order(actual_order, new_order, expected_query, experiment):
    subject_groups = baker.make(SubjectGroup, experiment=experiment,
                                _quantity=7, order=cycle(range(7)))
    update_subject_group(subject_groups[actual_order], order=new_order)

    assert list(SubjectGroup.objects.values_list('order', flat=True)
                .order_by('id')) == expected_query
