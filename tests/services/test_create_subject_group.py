import pytest
from model_bakery import baker

from huscy.recruitment.models import RecruitmentCriteria, SubjectGroup
from huscy.recruitment.services import create_subject_group

pytestmark = pytest.mark.django_db


def test_create_subject_group(experiment):
    assert not SubjectGroup.objects.exists()
    assert not RecruitmentCriteria.objects.exists()

    result = create_subject_group(experiment, 'name', 'description')

    subject_group = SubjectGroup.objects.filter(experiment=experiment).get()
    assert subject_group == result
    assert subject_group.experiment == experiment
    assert subject_group.name == 'name'
    assert subject_group.description == 'description'
    assert subject_group.order == 0
    assert subject_group.recruitment_criteria.get().attribute_filterset == {}


def test_create_second_subject_group_for_experiment(experiment):
    baker.make('recruitment.SubjectGroup', experiment=experiment)

    subject_group = create_subject_group(experiment, 'name', 'description')

    assert subject_group.order == 1
