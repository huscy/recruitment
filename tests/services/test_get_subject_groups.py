import pytest
from model_bakery import baker

from django.db.models import QuerySet

from huscy.recruitment import services

pytestmark = pytest.mark.django_db


def test_get_subject_groups(mocker, experiment):
    create_subject_group = mocker.spy(services, 'create_subject_group')

    baker.make('recruitment.SubjectGroup', experiment=experiment, _quantity=3)

    result = services.get_subject_groups(experiment)

    assert isinstance(result, QuerySet)
    assert len(result) == 3

    create_subject_group.assert_not_called()


def test_create_subject_group(mocker, experiment):
    create_subject_group = mocker.spy(services, 'create_subject_group')

    result = services.get_subject_groups(experiment)

    assert isinstance(result, QuerySet)
    assert len(result) == 1

    create_subject_group.assert_called_once_with(experiment=experiment, name='Subject group 1')
