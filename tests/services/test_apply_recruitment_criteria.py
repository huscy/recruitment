from datetime import date
from itertools import cycle

import pytest
from model_bakery import baker

from huscy.pseudonyms.services import create_pseudonym
from huscy.recruitment.services.attribute_filtersets import (
    apply_recruitment_criteria,
    filter_attributesets_by_attribute_filterset,
    filter_subjects_by_age,
)
from huscy.subjects.models import Subject

pytestmark = pytest.mark.django_db


@pytest.fixture
def subjects():
    return [
        baker.make(Subject, contact__date_of_birth=date(1965, 1, 1)),  # 60
        baker.make(Subject, contact__date_of_birth=date(1980, 1, 1)),  # 45
        baker.make(Subject, contact__date_of_birth=date(1991, 1, 1)),  # 34
        baker.make(Subject, contact__date_of_birth=date(1994, 1, 1)),  # 31
        baker.make(Subject, contact__date_of_birth=date(1999, 1, 1)),  # 26
        baker.make(Subject, contact__date_of_birth=date(2000, 1, 1)),  # 25
        baker.make(Subject, contact__date_of_birth=date(2004, 1, 1)),  # 21
        baker.make(Subject, contact__date_of_birth=date(2008, 1, 1)),  # 17
        baker.make(Subject, contact__date_of_birth=date(2012, 1, 1)),  # 13
        baker.make(Subject, contact__date_of_birth=date(2022, 10, 15)),  # 14 months
    ]


@pytest.fixture
def pseudonyms(subjects, attributeset_content_type):
    return [create_pseudonym(s, content_type=attributeset_content_type) for s in subjects]


@pytest.fixture
def attribute_schema():
    return baker.make('attributes.AttributeSchema')


@pytest.fixture
def attribute_sets(pseudonyms, attribute_schema):
    attribute_sets = [
        {
            'handedness': 'left',
            'health': {'height': 176, 'sex': 'female'},
            'languages': {'mother tongues': ['English', 'German']},
        },  # 60
        {
            'handedness': 'both',
            'health': {'height': 164, 'sex': 'female'},
            'languages': {'mother tongues': ['German']},
        },  # 45
        {
            'handedness': 'left',
            'health': {'height': 184, 'sex': 'male'},
            'languages': {'mother tongues': ['English']},
        },  # 34
        {'handedness': 'right', 'health': {'height': 165, 'sex': 'female'}},  # 31
        {'handedness': 'right', 'health': {'height': 192, 'sex': 'male'}},    # 26
        {'handedness': 'left', 'health': {'height': 176, 'sex': 'female'}},   # 25
        {'handedness': 'both', 'health': {'height': 164, 'sex': 'female'}},   # 21
        {'handedness': 'left', 'health': {'height': 184, 'sex': 'male'}},     # 17
        {'handedness': 'right', 'health': {'height': 165, 'sex': 'female'}},  # 13
        {'handedness': 'right', 'health': {'height': 192, 'sex': 'male'}},
    ]

    return baker.make('attributes.AttributeSet', attribute_schema=attribute_schema,
                      pseudonym=cycle([pseudonym.code for pseudonym in pseudonyms]),
                      attributes=cycle(attribute_sets), _quantity=10)


def test_apply_recruitment_criteria(subjects, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        minimum_age_in_months=21*12,
        maximum_age_in_months=40*12,
        attribute_filterset={
            'handedness': {
                'operator': 'exact',
                'values': ['left', 'both'],
            },
            'health__sex': {
                'operator': '-exact',
                'values': ['male'],
            }
        }
    )

    result = apply_recruitment_criteria(recruitment_criteria).order_by('id')

    assert len(result) == 2
    assert result.filter(id=subjects[5].id).exists()
    assert result.filter(id=subjects[6].id).exists()


def test_filter_attributesets_by_single_value(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'handedness': {'operator': 'exact', 'values': ['left']}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 4
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[7].code).exists()


def test_filter_attributesets_by_multi_values(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'handedness': {'operator': 'exact', 'values': ['left', 'both']}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 6
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()
    assert result.filter(pseudonym=pseudonyms[7].code).exists()


def test_filter_attributesets_categorized_attribute_filter(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__sex': {'operator': 'exact', 'values': ['female']}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 6
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[3].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()
    assert result.filter(pseudonym=pseudonyms[8].code).exists()


def test_filter_attributesets_lte_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': 'lte', 'values': [175]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 4
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[3].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()
    assert result.filter(pseudonym=pseudonyms[8].code).exists()


def test_filter_attributesets_gte_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': 'gte', 'values': [165]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 8
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()
    assert result.filter(pseudonym=pseudonyms[3].code).exists()
    assert result.filter(pseudonym=pseudonyms[4].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[7].code).exists()
    assert result.filter(pseudonym=pseudonyms[8].code).exists()
    assert result.filter(pseudonym=pseudonyms[9].code).exists()


def test_filter_attributesets_range_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': 'range', 'values': [[165, 180]]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 4
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[3].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[8].code).exists()


def test_filter_attributesets_contains_operator_with_single_value(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'languages__mother tongues': {'operator': 'contains', 'values': ['English']}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 2
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()


def test_filter_attributesets_contains_operator_with_multi_value(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'languages__mother tongues': {'operator': 'contains', 'values': [['German', 'English']]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 1
    assert result.filter(pseudonym=pseudonyms[0].code).exists()


def test_filter_attributesets_negate_exact_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'handedness': {'operator': '-exact', 'values': ['right']}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 6
    assert result.filter(pseudonym=pseudonyms[0].code).exists()
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()
    assert result.filter(pseudonym=pseudonyms[5].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()
    assert result.filter(pseudonym=pseudonyms[7].code).exists()


def test_filter_attributeset_negate_lte_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': '-lte', 'values': [190]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 2
    assert result.filter(pseudonym=pseudonyms[4].code).exists()
    assert result.filter(pseudonym=pseudonyms[9].code).exists()


def test_fiter_attributeset_negate_gte_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': '-gte', 'values': [165]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 2
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()


def test_filter_attributes_negate_range_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'health__height': {'operator': '-range', 'values': [[165, 180]]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 6
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()
    assert result.filter(pseudonym=pseudonyms[4].code).exists()
    assert result.filter(pseudonym=pseudonyms[6].code).exists()
    assert result.filter(pseudonym=pseudonyms[7].code).exists()
    assert result.filter(pseudonym=pseudonyms[9].code).exists()


def test_filter_attributes_negate_contains_operator(pseudonyms, attribute_sets):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        attribute_filterset={
            'languages__mother tongues': {'operator': '-contains', 'values':
                                          [['German', 'English']]}
        }
    )

    result = filter_attributesets_by_attribute_filterset(recruitment_criteria)

    assert len(result) == 2
    assert result.filter(pseudonym=pseudonyms[1].code).exists()
    assert result.filter(pseudonym=pseudonyms[2].code).exists()


def test_filter_subjects_by_age(subjects):
    recruitment_criteria = baker.make(
        'recruitment.RecruitmentCriteria',
        minimum_age_in_months=21*12,
        maximum_age_in_months=40*12,
    )
    subjects_queryset = Subject.objects.all()
    result = filter_subjects_by_age(subjects_queryset, recruitment_criteria)

    assert len(result) == 5
    assert result.filter(id=subjects[2].id).exists()
    assert result.filter(id=subjects[3].id).exists()
    assert result.filter(id=subjects[4].id).exists()
    assert result.filter(id=subjects[5].id).exists()
    assert result.filter(id=subjects[6].id).exists()
