import pytest

from huscy.recruitment.serializer import RecruitmentCriteriaSerializer, SubjectGroupSerializer

pytestmark = pytest.mark.django_db


def test_expose_recruitment_criteria(subject_group, recruitment_criteria):
    data = SubjectGroupSerializer(subject_group).data

    assert len(data['recruitment_criteria']) == 1

    serializer = RecruitmentCriteriaSerializer([recruitment_criteria], many=True)
    assert data['recruitment_criteria'] == serializer.data


def test_recruitment_criteria_is_read_only(subject_group, recruitment_criteria):
    subject_group = dict(
        recruitment_criteria=RecruitmentCriteriaSerializer([recruitment_criteria], many=True).data,
        description='description',
        name='name',
        experiment=subject_group.experiment.pk,
    )
    serializer = SubjectGroupSerializer(data=subject_group)
    serializer.is_valid()
    assert 'recruitment_criteria' not in serializer.validated_data
