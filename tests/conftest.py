from datetime import datetime, timedelta

import pytest
from model_bakery import baker

from django.contrib.contenttypes.models import ContentType

from rest_framework.test import APIClient


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Clara', last_name='Himmel')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
def content_type_contact_history():
    return ContentType.objects.get(app_label='recruitment', model='contacthistory')


@pytest.fixture
def attribute_schema():
    schema = {
        'type': 'object',
        'properties': {
            'attribute': {'type': 'number'}
        },
    }
    return baker.make('attributes.AttributeSchema', schema=schema)


@pytest.fixture
def subject():
    return baker.make('subjects.Subject')


@pytest.fixture
def project():
    return baker.make('projects.Project')


@pytest.fixture
def experiment(project):
    return baker.make('project_design.Experiment', project=project)


@pytest.fixture
def subject_group(experiment):
    return baker.make(
        'recruitment.SubjectGroup',
        experiment=experiment,
        name='name',
        description='description',
    )


@pytest.fixture
def recruitment_criteria(subject_group):
    return baker.make(
        'recruitment.RecruitmentCriteria',
        subject_group=subject_group,
        minimum_age_in_months=18*12,
        maximum_age_in_months=45*12,
    )


@pytest.fixture
def attributeset_content_type():
    return ContentType.objects.get(app_label='attributes', model='attributeset')


@pytest.fixture
def participation_request_content_type():
    return ContentType.objects.get(app_label='recruitment', model='participationrequest')


@pytest.fixture
def participation_request_pseudonym(subject, recruitment_criteria,
                                    participation_request_content_type):
    return baker.make('pseudonyms.Pseudonym', subject=subject,
                      content_type=participation_request_content_type,
                      object_id=recruitment_criteria.subject_group.experiment_id)


@pytest.fixture
def participation_request(subject, recruitment_criteria, participation_request_pseudonym):
    return baker.make('recruitment.ParticipationRequest', recruitment_criteria=recruitment_criteria,
                      pseudonym=participation_request_pseudonym.code)


@pytest.fixture
def session(project):
    return baker.make('projects.Session', experiment__project=project, duration=timedelta(hours=1))


@pytest.fixture
def timeslot(session):
    baker.make('appointments.Resource', name='a111')
    baker.make('projects.DataAcquisitionMethod', session=session, location='a111')
    return baker.make('bookings.Timeslot', session=session, active=True,
                      start=datetime(2020, 1, 1, 10))
