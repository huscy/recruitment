import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'huscy',
        'USER': 'huscy',
        'PASSWORD': '123',
        'HOST': os.environ.get('HUSCY_DATABASE_HOST', 'localhost'),
        'PORT': '5432',
    }
}


DEBUG = True


HUSCY = {
    'recruitment': {
        'default_age_range': (18*12, 40*12),
    },
}


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'guardian',
    'rest_framework',
    'rest_framework_nested',

    'huscy.appointments.apps.HuscyApp',
    'huscy.attributes.apps.HuscyApp',
    'huscy.bookings.apps.HuscyApp',
    'huscy.participations.apps.HuscyApp',
    'huscy.project_design.apps.HuscyApp',
    'huscy.projects.apps.HuscyApp',
    'huscy.pseudonyms.apps.HuscyApp',
    'huscy.recruitment.apps.HuscyApp',
    'huscy.subjects.apps.HuscyApp',
]

# check if django extensions is installed because it's just an extras_require dependency
try:
    import django_extensions
    INSTALLED_APPS.append('django_extensions')
except ImportError:
    pass


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'config.urls'


SECRET_KEY = '!!!SETUP DJANGO_SECRET_KEY!!!'


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Internationalization
USE_L10N = True

USE_TZ = False
